package club.jdiy.business.dao;

import club.jdiy.business.entity.Simple;
import club.jdiy.core.base.JDiyDao;
import org.springframework.stereotype.Repository;

@Repository
public interface SimpleDao extends JDiyDao<Simple, Long> {
}
