package club.jdiy.business.service.impl;

import club.jdiy.core.base.JDiyBaseService;
import club.jdiy.business.dao.SimpleDao;
import club.jdiy.business.entity.Simple;
import club.jdiy.business.service.SimpleService;
import club.jdiy.utils.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SimpleServiceImpl extends JDiyBaseService<Simple, SimpleDao, Long>
        implements SimpleService {


    /**
     * 实体的修改表单，业务保存方法。
     *
     * @param vo 表单页面传过来的数据
     * @return 持久化后的实体
     */
    @Override
    @Transactional
    public Simple logicSave(Simple vo) {
        Simple po = dao.findById(vo.getId()).orElse(new Simple());

        BeanUtils.copy(vo, po);

        return dao.save(po);
    }

}
