package club.jdiy.business.dao;

import club.jdiy.business.entity.Category;
import club.jdiy.core.base.JDiyDao;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryDao extends JDiyDao<Category, Integer> {

    //里面啥也没有写。普通的增删改查，写一个空的Dao足矣～～

    //   (这只是一个演示。主要是编码开发用到。  通过在线开发生成的界面根本不需要这个东西)

}
