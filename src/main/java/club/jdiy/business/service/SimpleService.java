package club.jdiy.business.service;

import club.jdiy.core.base.JDiyService;
import club.jdiy.business.dao.SimpleDao;
import club.jdiy.business.entity.Simple;

public interface SimpleService extends JDiyService<Simple, SimpleDao, Long> {
}
