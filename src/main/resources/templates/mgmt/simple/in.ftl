<#include "/templates/common/jdiy_macros.ftl"/>
<@body local=true>
    <form class="layui-form" id="form${_}" lay-filter="form${_}" action="${ctx}/mgmt/${modelName}/save" method="post"
          style="padding: 20px 30px 0 0;">
        <input type="hidden" name="id" value="${vo.id}"/>
        <@formRow>
            <@formItem label='名称' col=1>
                <@input name='name' verify='required'/>
            </@formItem>
        </@formRow>
        <@formRow>
            <@formItem label='内容说明' col=1>
                <@textarea name="content" style="height:120px;"/>
            </@formItem>
        </@formRow>
        <@formRow>
            <@formItem label='排序索引' col=2>
                <@input name='sortIndex' verify="required|number" placeholder="输入数字，越小越靠前"/>
            </@formItem>
            <@formItem label='日期框' col=2>
                <@input name='theDate' verify="required|date" class="fmt_date"/>
            </@formItem>
        </@formRow>
        <@formRow>
            <@formItem label='金额(小数)' col=2>
                <@input name='amount' verify="required|decimal"/>
            </@formItem>
            <@formItem label='是否启用' col=2>
                <input type="radio" name="enabled" value="true" title="启用"<#if vo.enabled> checked</#if>>
                <input type="radio" name="enabled" value="false" title="不启用"<#if !vo.enabled> checked</#if>>
            </@formItem>
        </@formRow>
        <div style="display: none;"><@submit/></div>
    </form>
</@body>
<@footer>
    <script>


        layui.use([], function () {
            var form = layui.form;

            form.on('submit(submit${_})', function (data) {
                //js验证
                return jdiyAdmin.ajaxSubmit(data.form);
            });
        })
    </script>
</@footer>
