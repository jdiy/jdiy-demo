<#include "/templates/common/jdiy_macros.ftl"/>
<@body>
    <form class="layui-form" id="form${_}" lay-filter="form${_}" action="${ctx}/mgmt/appConfig/doSave"
          method="post" enctype="multipart/form-data" style="padding: 20px 30px 0 0;">
        <@card>
            <@cardHead>
                系统设置
            </@cardHead>
            <@cardBody>
                <#assign labelstyle=''/>
                <#macro confLine label tips='' inputWidth=400>
                    <@formRow>
                        <label class="layui-form-label" style="width:105px;${labelstyle}">${label}：</label>
                        <div class="layui-input-inline" style="width:${inputWidth}px;">
                            <#nested />
                        </div>
                        <#if tips!=''>
                            <div class="layui-form-mid layui-word-aux">${tips}</div></#if>
                    </@formRow>
                </#macro>

                <@confLine label="这是一个开关" tips='此页面为自定义系统配置页面'>
                    <input type="radio" name="tjTestOn" value="true" title="开启"<#if vo.tjTestOn> checked</#if>>
                    <input type="radio" name="tjTestOn" value="false" title="关闭"<#if !vo.tjTestOn> checked</#if>>
                </@confLine>


                <@confLine label="这是一个文字" tips='此页面为自定义系统配置页面'>
                    <@input name="someConf"/>
                </@confLine>

                <@confLine label="数字" tips='只能填数字。 这只是一个演示，没啥意义' inputWidth=150>
                    <@input name="testInt" verify='required|number'/>
                </@confLine>
                <@confLine label="日期格式" tips='只能填日期' inputWidth=150>
                    <@input name="testDate" verify='required|date' class="fmt_date"/>
                </@confLine>

            </@cardBody>
        </@card>

        <div><@submit value="　　保存修改　　" show=true/></div>
    </form>
</@body>
<@footer>
    <script>

        layui.use([], function () {
            var form = layui.form;

            form.on('submit(submit${_})', function (data) {

                return jdiyAdmin.ajaxSubmit(data.form);
            });
        })
    </script>
</@footer>