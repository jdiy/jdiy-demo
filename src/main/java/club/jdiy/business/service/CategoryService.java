package club.jdiy.business.service;

import club.jdiy.business.dao.CategoryDao;
import club.jdiy.business.entity.Category;
import club.jdiy.core.base.JDiyService;

//[文章栏目] service接口层。
// 一个空壳（与Dao一样，同样集成了完善的增删改查内置方法）。注意extends后面的内容。
public interface CategoryService
        extends JDiyService<Category, CategoryDao, Integer> {

}
