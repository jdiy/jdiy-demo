package club.jdiy.business.service.impl;

import club.jdiy.admin.dao.UserDao;
import club.jdiy.core.base.JDiyBaseService;
import club.jdiy.business.dao.ArticleDao;
import club.jdiy.business.entity.Article;
import club.jdiy.business.service.ArticleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 *   文章 Service实现层      (这只是一个演示。主要是编码开发用到。  通过在线开发生成的界面不需要这个东西)
 */
@Service
public class ArticleServiceImpl
        extends JDiyBaseService<Article, ArticleDao, Integer>
        implements ArticleService {
    //注意Service实现层的 extends后面的内容，固定写法，JDiyBaseService后面第1个泛型是实体类，第2个是该实体的Dao接口, 第3个是实体的主键类型
    //它的implements接口层就是把extends JDiyBaseService改成extends JDiyService, 详见ArticleService

    //业务方法实现类。
    public void foobar(Article article) {
        //.................    业务代码略
        //当前实体的dao直接使用：
        dao.save(article);
        //..................   让您专注于编写业务逻辑
    }

    //注入其它实体的dao:
    @Resource
    private UserDao userDao;
    //...
}
