package club.jdiy.business.entity;

import club.jdiy.core.base.DslFilter;
import club.jdiy.core.base.domain.DslFilterable;
import club.jdiy.core.base.domain.JpaEntity;
import club.jdiy.core.base.domain.Sortable;
import com.querydsl.core.BooleanBuilder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * 所在地区(树形实体/表)
 * <p>
 * (这只是一个演示。主要是编码开发用到。  通过在线开发生成的界面不需要这个东西. )
 * <p>
 * 本人开发项目更多的时候是直接建一个实体类，然后运行程序，通过实体类来自动生成数据库表（从不手动建表）
 * 然后
 * 如果不需要编码开发，就直接通过在线开发生成管理界面（此时代码里面只有一个实体类）;
 * 如果还需要编码开发（如要写API接口，或写后台自定义页面，或其它业务逻辑，或是各种界面增强器时），在有需要时，才去创建此实体对应的 Dao/Service/Controller层等
 */
@Data
@Entity
@Table(name = "b_area")
@NoArgsConstructor
public class Area
        implements JpaEntity<String>, DslFilterable, Sortable {
//注意实体类的implements部分。所有实体类都是JpaEntity<T>  T=这个实体的主键类型  后面其它的DslFilterable, Sortable之类，不是必须的(根据实际需要)。

    @Id
    @Column(columnDefinition = "char(10)")
    private String id;//地区ID(此处定义为字符串型的主键了。)
    @Column(length = 64)
    private String name;

    @ManyToOne
    @JoinColumn(name = "parentId", foreignKey = @ForeignKey(name = "FK_area_parent"))
    private Area parent;//上级地区

    @OneToMany(mappedBy = "parent")
    @OrderBy("sortIndex ASC")
    private List<Area> children;//子级地区

    @Column(columnDefinition = "text", nullable = false)
    private String sortPath;

    @Column(name = "sortIndex")
    private Integer sortIndex;//排序索引（数字小靠前）

    public Area(String id) {
        this.id = id;
    }

    @PreUpdate
    @PrePersist
    private void init() {
        if (sortIndex == null) sortIndex = 100;
    }

    @Override
    public DslFilter createFilter(BooleanBuilder builder) {
        QArea qo = QArea.area;
        if (parent != null && parent.getId() != null) builder.and(qo.parent.id.eq(parent.id));
        return new DslFilter(builder,
                qo.id.desc(), qo.name.asc());
    }
}
