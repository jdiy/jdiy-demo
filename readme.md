# JDiyAdmin演示程序

jdiy-admin 是一个后台管理低代码开发平台。 可视化的在线开发配置(鼠标拖拉拽快速创建管理界面)，且配置实时生效无需编译重启。 复杂业务还可以通过JDiy的各类界面增强器，整合可编程开发，实现“在线开发”与普通后端编程完美融合。  
当前demo程序提供了一些后台界面在线开发配置样例，仅供参考.

## IDEA开发环境搭建运行步骤：

1. **使用Intellij IDEA打开:**  
   使用IDEA开发工具直接打开本项目．首次打开后请调出IDEA右侧Maven面版,点击Maven面板的＂刷新＂按钮，让IDEA自动下载相关pom依赖包.


2. **还原数据库：**  
   项目根目录下DB子目录内有一个jdiy-demo.sql为mysql数据库备份文件，将其还原至mysql或Mariadb. (建议mysql版本不低于5.6)


3. **修改数据库连接及端口号：**  
   修改src/main/resource/application.yml中的mysql数据库连接地址/端口/登录帐号和密码．


4. JDiy目前支持三种类型的缓存配置（任选其一：memcached, redis, none）

    * 若为memcached (推荐, 默认使用)，   
      则在application.yml中配置(示例如下)：
        ```
       jdiy:
           cache:
              type: memcached
              memcached:
                  servers: 127.0.0.1:11211
       ```

    + 若为redis，则配置(示例)：
       ```
       jdiy:
         cache:
            type: redis
            redis:
               servers: 127.0.0.1:6379
               password: 若有密码则加此行，这儿填密码.
       ```
    + none 表示不使用外部缓存，也就是直接使用程序内部的缓存（不推荐），则配置：
       ```
       jdiy:
         cache:
            type: none
       ```

5. IDEA开发环境下启动运行：  
   首先，调出IDEA右侧`Maven`面板，展开`jdiy-demo` > ` Lifecyce`  双击里面的 `compile` 编译项目源码.  
   然后，运行com.jdiy.business.MainApplication 即可启动项目．    
   启动完成后，浏览器输入如下网址登录后台：   
   http://localhost:8886/mgmt/   
   开发者帐号默认为： jdiy 密码： jdiy888

## 如何修改开发者帐号?

开发者帐号拥有＂在线开发＂功能，它是系统的最高权限帐号（高于超级管理员）

+ 首先，开发者必须是超级管理员，即需要先在后台创建或修改用户帐号，并赋予其超级管理员角色.
+ 修改application.yml配置文件， 如下：
  ```
  jdiy:
     developers: jdiy, system
  ```

此配置，表示设置jdiy和system这两个帐号成为开发者．  
设置后重启项目，并用新帐号重新登录进后台即可生效．


> 注： 此文档只是简述了开发环境运行搭建过程，它与普通的SpringBoot程序并无太大区别。
> 更多的JDiy使用教程还在进一步完善之中，我们会在JDiy官网逐步开放相关技术文档和入门教程。
>
> JDiy官网： http://jdiy.club  
> 作者： 子秋 (ziquee   QQ: 39886616   QQ群：7759217)