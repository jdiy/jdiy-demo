package club.jdiy.business;

import club.jdiy.core.base.JDiyBaseRepository;
import club.jdiy.core.AppContext;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;

@SpringBootApplication
@ServletComponentScan
@EnableTransactionManagement
@EntityScan({"club.jdiy.**"})
@ComponentScan({"club.jdiy.**"})
@EnableJpaRepositories(repositoryBaseClass = JDiyBaseRepository.class,
        basePackages = {"club.jdiy.**"})
public class MainApplication {

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }

    @Bean
    public ApplicationRunner afterStartup() {
        return args -> {
           //启动完成后可执行something.
        };
    }

    @Bean
    public CommandLineRunner commandLineRunner() {
        return strings -> {
            System.out.println("程序已启动．");//todo nothing
        };
    }

    @Resource
    private AppContext context;
}

