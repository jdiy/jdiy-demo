package club.jdiy.business.entity;

import club.jdiy.core.base.JpaFilter;
import club.jdiy.core.base.WhereBuilder;
import club.jdiy.core.base.domain.DslFilterable;
import club.jdiy.core.base.domain.JpaEntity;
import club.jdiy.core.base.domain.JpaFilterable;
import club.jdiy.core.base.domain.Removable;
import club.jdiy.utils.DateUtils;
import club.jdiy.utils.StringUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * 简单实体，演示用。   没有什么业务上的意议
 */
@Entity
@Table(name = "b_simple")
@Data
@NoArgsConstructor
public class Simple
        implements JpaEntity<Long>, JpaFilterable {
    //其它实体的动态查询都是用的DslFilterable, 这儿演示使用JpaFilterable(另一种动态查询支持)

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String content;

    @Column(columnDefinition = "int default 100 not null")
    private Integer sortIndex;//int

    private LocalDate theDate;//日期

    @Column(columnDefinition = "decimal(16,2)")
    private Double amount;//double

    //是否启用
    // 演示boolean类型
    // (在JDiy在线开发相关配置界面，值用0和1表示， 0=false 1=true.  因为mysql的boolean本身就是 bit(1)类型，到数据库中就是1和0)
    private Boolean enabled;

    //非持久化字段，这儿只是为了做时间范围搜索的动态查询
    @Transient
    private LocalDate theDate_;

    //不允许为空的，可以加这个方法，以编程方式设置默认值，否则就只能在添加数据的业务方法中去设置了（容易漏写)。   这是JPA中的知识
    @PreUpdate
    @PrePersist
    private void init() {
        if (sortIndex == null) sortIndex = 100;
    }

    public Simple(Long id) {
        this.id = id;
    }


    //特别注意：
    // 注意其它实体（如Article）是 implements DslFilterable
    //                而这儿却是 implements JpaFilterable
    // 这只是为了演示另一种动态查询的使用方法。
    // 但我们不推荐用这种写法，因为它是直接将JPQL以字符串的形式拼接的。
    //虽然能做到"pre-statement"（避免sql注入）但它不是Type Safe类型安全的。
    //所以，能用DslFilter的，尽量不要使用JpaFilter吧。。。。。
    @Override
    public JpaFilter createFilter(WhereBuilder builder) {
        //o表示当前实体（固定写法）
        if (StringUtils.hasText(name)) builder.append("and o.name=?1", name.trim());
        if (StringUtils.hasText(content)) builder.append("and o.content like concat('%',?1,'%')", content.trim());
        if (theDate != null) builder.append("and o.theDate>='" + DateUtils.fmt_date.format(theDate) + "'");
        if (theDate_ != null) builder.append("and o.theDate<='" + DateUtils.fmt_date.format(theDate_) + "'");
        if (enabled != null) builder.append("and o.enabled=?1", enabled);
        return new JpaFilter(this.getClass(), builder, "o.id desc");
    }
}
