package club.jdiy.business.mgmt;

import club.jdiy.admin.controller.JDiyAdminCtrl;
import club.jdiy.business.entity.Simple;
import club.jdiy.business.service.SimpleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "mgmt/simple")
public class SimpleMgmt extends JDiyAdminCtrl<Simple, SimpleService> {

}
