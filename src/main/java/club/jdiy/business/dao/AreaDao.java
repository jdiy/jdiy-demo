package club.jdiy.business.dao;

import club.jdiy.business.entity.Area;
import club.jdiy.core.base.JDiyDao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 地区DAO   不需要写impl实现类噢        (这只是一个演示。主要是编码开发用到。  通过在线开发生成的界面不需要这个东西)
 */
@Repository
public interface AreaDao
        extends JDiyDao<Area, String> {
    //注意Dao层接口的 extends后面的内容，固定写法，JDiyDao后面第1个泛型是实体类，第2个是该实体的主键类型
    //如果这个Dao有实现层，就把 JDiyDao换成 JDiyBaseDao

    @Query("SELECT o from Area o where o.parent is null order by o.sortIndex")
    List<Area> findRoots();


    @Query("select o from Area o where o.parent.id=?1 order by o.sortIndex")
    List<Area> findChildren(String parentId);

}
