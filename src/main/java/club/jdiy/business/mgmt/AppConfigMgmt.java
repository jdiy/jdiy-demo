package club.jdiy.business.mgmt;

import club.jdiy.admin.interceptor.GuestDisabled;
import club.jdiy.core.AppContext;
import club.jdiy.core.base.Java8MyDateEditor;
import club.jdiy.core.base.domain.Ret;
import club.jdiy.business.AppConfig;
import club.jdiy.utils.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 这仅仅是一个演示。   主要是编码开发用到。  通过在线开发生成的界面不需要这个东西
 *
 * for：管理后台的“系统管理-系统设置”模块。
 */
@Controller
@RequestMapping(value = "/mgmt/appConfig")
public class AppConfigMgmt {
    //后台的全局变量配置页面Controller.（即管理后台的“系统配置”模块，配置内容由您自行根据业务系统需要开发）
    // 这儿它比较特殊，没有使用其它Controller那样常规的extends写法


    //自已定义一个页面，让用户进行相应的全局配置。
    @RequestMapping("form")
    public String form(ModelMap map) {

        //AppConfig是用户自建的一个类，用来存储全局配置信息的一个Model。您可自行在这个类中增加其它系统配置字段。
        //通过如下方法，轻松获取全局变量:
        AppConfig appConfig = conetxt.getConfig(AppConfig.class);

        //map到view层,让用户配置：
        map.put("vo", appConfig);
        return "mgmt/appConfig/form.ftl";
    }


    //下面是系统配置的doSave保存方法
    @GuestDisabled
    @RequestMapping("doSave")
    @ResponseBody
    public Ret<?> doSave(AppConfig vo) {//vo是用户表单提交过来的信息。
        try {
            AppConfig po = conetxt.getConfig(AppConfig.class);//读取系统配置。

            //一键将用户修改的信息，复制到配置Model：
            BeanUtils.copyNotNull(vo, po);

            //全局配置变量 持久化保存：
            conetxt.saveConfig(po);

            return Ret.success();
        } catch (Exception le) {
            return Ret.error(le);
        }
    }


    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(LocalDateTime.class, new Java8MyDateEditor(Java8MyDateEditor.Type.LocalDateTime));
        binder.registerCustomEditor(LocalDate.class, new Java8MyDateEditor(Java8MyDateEditor.Type.LocalDate));
    }

    @Resource
    private AppContext conetxt;

}
