package club.jdiy.business.dao;

import club.jdiy.core.base.JDiyDao;
import club.jdiy.business.entity.Article;
import club.jdiy.business.entity.Category;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Article文章 Dao层代码。  注意90%的Dao，不需要写实现层（当然实现层也是可以有的。）
 * <p>
 * (这只是一个演示。主要是编码开发用到。  通过在线开发生成的界面不需要这个东西)
 */
@SuppressWarnings("unused")
@Repository
public interface ArticleDao extends JDiyDao<Article, Integer> {
    //spring-data-jpa 方法推导查询示例
    // “推导”的意思是，自动根据方法名称的字面意思，做相应的推导自动完成查询。
    // 它不需要写任何方法的实现，也不需要sql/jql之类的Query语句，就在这个接口中定义一个方法即可。
    // 当然方法名称的书写有一定的规则要求(不懂的补习一下springDataJPA的知识吧)
    // 下面这个方法，就字面意思，应该一看就懂，即按标题查询Article,并返回匹配的记录中的第1条：
    Article findFirstByTitle(String title);

    //又如：
    List<Article> findAllByTitleLike(String key);

    List<Article> findByCategory(Category category);

    boolean existsByTitle(String title);

    //spring-data-jpa 推导删除演示（修改删除要加 @Modifying 注解）
    @Modifying
    boolean removeAllByTitleLike(String key);

    //按Query注解查询示例:
    @Query("select o from Article o where o.category.id=?1 and o.author=?2")
    List<Article> testQuery(Integer categoryId, String name);

    //分类统计查询示例:演示统计各文章栏目下的文章数:
    // 返回统计结果，统计结果不是一个实体类 (需要按下面查询语句中所示的包下建立一个SomeTjVo并有构造方法：SomeTjVo(String, Long)
    //@Query("select new club.jdiy.vo.SomeTjVo(o.category.name, count(o)) from Article o group by o.category")
    //List<SomeTjVo> countAllCatetoryArtileNums();


    //不止于此，更复杂的查询还可以有impl实现类。  JPA EntityManager / QueryDSL / JDiy原生SqlDao 均已在实现类中内置
}
